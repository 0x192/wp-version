#!/usr/bin/python2
import requests, argparse, sqlite3, hashlib, sys
from time import sleep

def sqlLookup(query):
    sql = sqlite3.connect('wp_version_data.db')
    cur = sql.cursor()
    return cur.execute(query).fetchall()    

def getFileList():
    files = sqlLookup('SELECT name FROM file_data')
    unique_files = []
    for file in files:
        if not file[0] in unique_files:
            unique_files.append(file[0])
    return unique_files

def checkHash(digest):
    result = sqlLookup('SELECT * FROM file_data WHERE hash="%s"' % digest)
    if len(result) > 0:
        return result[0]
        
    return False    

def main():
    parser = argparse.ArgumentParser(
            description='Determine wordpress version of a website based on official api hashes of static content')
    parser.add_argument('address', metavar='addr', type=str,
            help='an address with full path to WP: https://blog.example.com')
    parser.add_argument('--quick', action='store_true', default=False, 
            help='Stop after first match')          
    parser.add_argument('--wait', metavar='N', type=int,
            help='Wait n seconds between requests')
    args = parser.parse_args()
    
    print 'URL:\t%s\nQUICK:\t%s\nWAIT:\t%s\n' % (args.address, args.quick, args.wait)

    for file in getFileList():
        print '\rChecking "%s"...' % file,
        r = requests.get(args.address + '/' + file)

        rec = checkHash(hashlib.md5(r.text.encode('utf-8')).hexdigest())
        if rec is not False:
            print 'FOUND'
            print '\tVersion: %s\n\tFileName: %s\n\tHash: %s' % (rec[3],rec[1],rec[2])
            if args.quick:
                break
        else:
            print '\r' + (' ' * 100),

        if args.wait is not None:
            sleep(int(args.wait))

if __name__ == "__main__":
    main()
