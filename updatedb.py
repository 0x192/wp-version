import requests, json, sqlite3, re

VERSIONS_URL = 'https://codex.wordpress.org/WordPress_Versions'
FILEDATA_URL = 'https://api.wordpress.org/core/checksums/1.0/?version=%s&locale=en_US'

def getVersions():    
    return re.findall(r'<b><a href="/V.+title="V.+">(.+)</a>', requests.get(VERSIONS_URL).content)   # jenic pls halp

def getFileData():
    hash_dict = {}
    unique_files = []
    for version in getVersions():
        j = json.loads(requests.get(FILEDATA_URL % version).content)
        checksums = j['checksums']
        if checksums:
            for k in checksums:
                if not checksums[k] in hash_dict:
                    hash_dict[checksums[k]] = {'filename': k, 'version': version, 'count': 1}
                else:
                    hash_dict[checksums[k]]['count'] += 1
        
    for hash in hash_dict:
        if hash_dict[hash]['count'] == 1:
            unique_files.append({
                'name': hash_dict[hash]['filename'], 
                'hash': hash,
                'version': hash_dict[hash]['version']
            })
           
    return unique_files

def update():
    sql = sqlite3.connect('wp_version_data.db')
    cur = sql.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS file_data(id INTEGER PRIMARY KEY, name TEXT, hash TEXT, version TEXT)')
    sql.commit()

    file_data = getFileData()
    for file in file_data:
        name = file['name']
        hash = file['hash']
        version = file['version']
        
        cur.execute('SELECT * FROM file_data WHERE name=? AND hash=?', (name, hash))
        if not cur.fetchone():
            cur.execute('INSERT INTO file_data(name, hash, version) VALUES(?,?,?)', (name, hash, version))
            sql.commit()