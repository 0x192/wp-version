# WP-Version

_Determine the version of wordpress running on a website_

**Required Modules:**

* argparse
* Requests
* hashlib
* time
* json

## Usage

    wp-version.py [-h] [--quick] [--wait N] addr
    
    Determine wordpress version of a website based on official api hashes of
    static content
    
    positional arguments:
      addr           an address with full path to WP: https://blog.example.com
    
    optional arguments:
      -h, --help     show this help message and exit
      --quick        Stop after first match
      --wait N       Wait n seconds between requests
      